package com.mijnmarkt.ruben.mijnmarkt;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DetailDownloader extends AsyncTask<Void, Void, DetailDownloader.discount> {

    /** Same functionality as the supermarketdownloader class, except this one is specially for the discounts per supermarket **/

    private HttpURLConnection mHttpUrl;
    private WeakReference<DetailActivity> _detailActivity;
    Integer _Id;


    public DetailDownloader(DetailActivity parent, Integer id) {
        super();
        _detailActivity = new WeakReference<DetailActivity>(parent);
        _Id = id;
    }

    @Override
    protected discount doInBackground(Void... voids) {
        return getDetailData();
    }

    @Override
    protected void onPostExecute(discount result) {

        // AsyncTask finishes here

        // check if link with parent isn't lost
        // and if a location has been found before returning

        if (result != null && _detailActivity.get() != null) {
            DetailActivity detailActivity = _detailActivity.get();
            detailActivity.setDetails(result);

        } else {
            Log.d("DETAIL_API_LOG", "ON POST EXECUTE NO RESULTS");
        }
    }

    private discount getDetailData() {
        String result = null;
        BufferedReader in = null;

        try {

//            Log.d("DETAIL_API_LOG", "REQUESTING DATA");
            URL url;
//            url = new URL("https://docent.cmi.hro.nl/bootb/service/v1/discount/");

            url = new URL("https://supermarket-api.rubenmelchers.nl/" + _Id);
            mHttpUrl = (HttpURLConnection) url.openConnection();
            mHttpUrl.setRequestMethod("GET");
            mHttpUrl.setRequestProperty("Accept", "*/*");

//            mHttpUrl.setReadTimeout(10000);

            in = new BufferedReader(new InputStreamReader(
                    mHttpUrl.getInputStream()));
            Log.d("DETAIL_API_LOG", "doing stuff");

            Log.d("DETAIL_API_LOG", "IN = " + in);

            StringBuffer sb = new StringBuffer("");
            String line = "";
            Log.d("DETAIL_API_LOG", "SB = " + sb);
            while ((line = in.readLine()) != null) {
//                Log.d("DETAIL_API_LOG", "writing");
                sb.append(line + "\n");
            }
            result = sb.toString();

        } catch (MalformedURLException e) {
            Log.d("DETAIL_API_LOG", "URLEXCEPTION" + e);
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("DETAIL_API_LOG", "IOEXCEPTION" + e);
            e.printStackTrace();
        } finally {
            Log.d("DETAIL_API_LOG", "GOT THROUGH");

            try {
                if (null != in) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            mHttpUrl.disconnect();
        }
//        Log.d("DETAIL_API_LOG", "GOT RESULTS" + result);
        return createJson(result);

    }

    private discount createJson(String result) {
        discount _discount = new discount();

        Log.d("DETAIL_API_LOG", "creating json...");

        try {
            JSONArray marketData = new JSONArray(result);

            for(int i = 0; i < marketData.length(); i++) {
                JSONObject row = marketData.getJSONObject(i);

//                Toast.makeText(getApplicationContext(),"Received ID: " + row.getString(""), Toast.LENGTH_SHORT).show();
                Log.d("DETAIL_API_LOG", "detailtitle = " + row.getString("title"));
                Log.d("DETAIL_API_LOG", "detaildiscounts = " + row.getString("discounts"));

                JSONArray discountrows = new JSONArray(row.getString("discounts"));

                for(int j = 0; j < discountrows.length(); j++ ) {
                    JSONObject drow = discountrows.getJSONObject(j);

                    Log.d("DETAIL_API_LOG", "GOT DETAIL DISCOUNT! - " + drow.getInt("id"));
                    _discount.getDetailId().add(drow.getInt("id"));
                    _discount.getTitle().add(drow.getString("title"));
                    _discount.getDescription().add(drow.getString("description"));
                    _discount.getLikes().add((float)drow.getDouble("likes"));
                    _discount.getCategory().add(drow.getString("category"));
                }
//                _discount.getId().add(row.getInt("id"));
//                _discount.getTitle().add(row.getString("title"));

//                _discount = new discount(row.getInt("id"), row.getString("title"), row.getString("description"), (float) row.getDouble("likes"), row.getString("category"));
            }

        } catch (JSONException e) {

            Log.d("DETAIL_API_LOG", "error: " + e);
            e.printStackTrace();

            _discount = new discount();
        }

        return _discount;
    }

    public static class discount{
        ArrayList<Integer> DetailID = new ArrayList<>();
        ArrayList<String> Title = new ArrayList<>();
        ArrayList<String> Description = new ArrayList<>();
        ArrayList<Float> Likes = new ArrayList<>();
        ArrayList<String> Category = new ArrayList<>();

        public ArrayList<Integer> getDetailId() { return DetailID; }
        public ArrayList<String> getTitle() { return Title; }
        public ArrayList<String> getDescription() { return Description; }
        public ArrayList<Float> getLikes() { return Likes; }
        public ArrayList<String> getCategory() { return Category; }

        public void setDiscountID(ArrayList<Integer> id) {
            DetailID = id;
        }
        public void setTitle(ArrayList<String> title) {
            Title = title;
        }
        public void setDescription(ArrayList<String> description) {
            Description = description;
        }
        public void setLikes(ArrayList<Float> likes) {
            Likes = likes;
        }
        public void setCategory(ArrayList<String> category) {
            Category = category;
        }


    }
}
