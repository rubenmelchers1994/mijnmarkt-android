package com.mijnmarkt.ruben.mijnmarkt;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import static android.support.constraint.Constraints.TAG;

public class Tracking extends Service{

    private Tracking _self = this;
    private LocationManager _locationManager;
    private boolean isRunning  = false;

    Double marketLat;
    Double marketLong;
    String marketTitle;

    Double myLat;
    Double myLong;

    public Tracking() {

    }


    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;

        _locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        isRunning = false;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {
//        Log.d("TRACKING_LOG", "onStartCommand");

        /** Get data from mapActivity **/
        marketLat = intent.getDoubleExtra("market_lat", 0);
        marketLong = intent.getDoubleExtra("market_long", 0);
        marketTitle = intent.getStringExtra("market_title");

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return START_STICKY;
        }

        _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("DISTANCE", "MOVING IN BACKGROUND");

                /** same logic for the mapsactivity, but this runs in the background **/
                myLat = location.getLatitude();
                myLong = location.getLongitude();

                Location myLocation = new Location("ownLocation");
                myLocation.setLatitude(myLat);
                myLocation.setLongitude(myLong);

                Location supermarketLocation = new Location("store");
                supermarketLocation.setLatitude(marketLat);
                supermarketLocation.setLongitude(marketLong);

                /** If used is within range, send a notification, informing the user **/
                Boolean distanceMeter = checkDistance(myLocation, supermarketLocation);
                if(distanceMeter)
                {
                    sendNotification();
                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                    Log.d("DISTANCE", "within range!");
                }
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onStatusChanged(String provider, int status,
                                        Bundle extras) {
            }
        });
        return START_STICKY;
    }

    /** Create and send notification. Add context to it and make clickable. **/
    public void sendNotification(){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.img_walking)
                .setContentTitle("Bijna bij " + marketTitle)
                .setContentText("je bent er bijna!");

        Intent resultIntent = new Intent(this, MapsActivity.class);
        resultIntent.putExtra("supermarket_latitude", marketLat);
        resultIntent.putExtra("supermarket_longitude", marketLong);
        resultIntent.putExtra("supermarket_title", marketTitle);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MapsActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    /** Check the distance difference between two locations **/
    private Boolean checkDistance(Location startPoint, Location endpoint)
    {
        Boolean isClose = false;
        Location start=startPoint;
        Location end=endpoint;
        double distance=start.distanceTo(end);
        if(distance < 200f)
        {
            isClose = true;
        }
        return isClose;
    }

}
