package com.mijnmarkt.ruben.mijnmarkt;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mijnmarkt.ruben.mijnmarkt.ItemFragment.OnListFragmentInteractionListener;
import com.mijnmarkt.ruben.mijnmarkt.dummy.DummyContent;
import com.mijnmarkt.ruben.mijnmarkt.dummy.DummyContent.DummyItem;
import com.mijnmarkt.ruben.mijnmarkt.dummy.DummyContent.Supermarket;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<Supermarket> mValues;

    public MyItemRecyclerViewAdapter(List<Supermarket> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("DEBUG", "method called oncreateviewholder");
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.supermarket_row_content, parent, false);
        return new ViewHolder(view);
    }

    /** Create supermarket item and make it clickable **/
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Log.d("DEBUG", "method called onBindViewHolder");

        holder.mItem = mValues.get(position);
        holder.mTitleView.setText(mValues.get(position).getName());
        holder.mStreetView.setText(mValues.get(position).getStreet());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();

                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("supermarket_id", holder.mItem.getId() + 1);
                intent.putExtra("supermarket_title", holder.mItem.getName());
                intent.putExtra("supermarket_street", holder.mItem.getStreet());
                intent.putExtra("supermarket_lat", holder.mItem.getLatitude());
                intent.putExtra("supermarket_long", holder.mItem.getlongitude());

                intent.putExtra("mItem.id", holder.mItem.id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mTitleView;
        public final TextView mStreetView;
        public Supermarket mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = view.findViewById(R.id.item_number);
            mTitleView = view.findViewById(R.id.supermarket_title);
            mStreetView = view.findViewById(R.id.supermarket_street);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleView.getText() + "'";
        }
    }
}
