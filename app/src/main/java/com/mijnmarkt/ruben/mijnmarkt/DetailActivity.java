package com.mijnmarkt.ruben.mijnmarkt;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class DetailActivity extends AppCompatActivity {

    TextView titleText;
    TextView streetText;
    TextView product;
    TextView description;
    TextView likes;

    private DetailDownloader.discount _discount = new DetailDownloader.discount();
    private ArrayAdapter<Integer> _adapterID;
    private ArrayAdapter<String> _adapterTitle;
    private ArrayAdapter<String> _adapterDescription;
    private ArrayAdapter<Float> _adapterLikes;
    private ArrayAdapter<String> _adapterCat;

    Double _latitude;
    Double _longitude;
    String _title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        titleText = findViewById(R.id.marketdetail_title);
        streetText = findViewById(R.id.marketdetail_street);
        product = findViewById(R.id.marketdetail_product);
        description = findViewById(R.id.marketdetail_description);
        likes = findViewById(R.id.marketdetail_likes);

        Intent intent = getIntent();

        Integer _id = intent.getIntExtra("supermarket_id", 0);
        _title = intent.getStringExtra("supermarket_title");
        String _street = intent.getStringExtra("supermarket_street");
        _latitude = intent.getDoubleExtra("supermarket_lat", 0);
        _longitude = intent.getDoubleExtra("supermarket_long", 0);

        Toast.makeText(getApplicationContext(),"Received ID: " + _id, Toast.LENGTH_SHORT).show();

        titleText.setText(_title);
        streetText.setText(_street);
        new DetailDownloader(this, _id).execute();

        _adapterID = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _discount.getDetailId());
        _adapterTitle = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _discount.getTitle());
        _adapterDescription = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _discount.getDescription());
        _adapterLikes = new ArrayAdapter<Float>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _discount.getLikes());
        _adapterCat = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _discount.getCategory());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("DEBUG_PERMISSIONS", "permissions granted");
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
    }

    /** Fill array with detailpage (discount) information and set the text for the elements **/
    public void setDetails(DetailDownloader.discount discount) {

        _discount = discount;
        _adapterID.addAll(discount.getDetailId());
        _adapterTitle.addAll(discount.getTitle());
        _adapterDescription.addAll(discount.getDescription());
        _adapterLikes.addAll(discount.getLikes());
        _adapterCat.addAll(discount.getCategory());

        _adapterID.notifyDataSetChanged();
        _adapterTitle.notifyDataSetChanged();
        _adapterDescription.notifyDataSetChanged();
        _adapterLikes.notifyDataSetChanged();
        _adapterCat.notifyDataSetChanged();

        product.setText("" + _adapterTitle.getItem(0));
        description.setText("" + _adapterTitle.getItem(0));
        likes.setText("" + (_adapterLikes.getItem(0)));

    }

    /** method called when clicking on the navigate button. This sends the user to the mapview to see how to get to the supermarket **/
    public void navigate(View view) {


        Toast.makeText(getApplicationContext(),"Navigating", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("supermarket_latitude", _latitude);
        intent.putExtra("supermarket_longitude", _longitude);
        intent.putExtra("supermarket_title", _title);

        startActivity(intent);
    }

}
