package com.mijnmarkt.ruben.mijnmarkt.dummy;

import android.util.Log;

import com.mijnmarkt.ruben.mijnmarkt.Supermarkets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    String url = "";

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static final List<Supermarket> SUPERMARKETS = new ArrayList<Supermarket>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();
    public static final Map<Integer, Supermarket> SUPERMARKET_MAP = new HashMap<Integer, Supermarket>();

    private static final int COUNT = 0;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addSupermarket(createSupermarket(i, "test", "teststraat", 20.0, 10.0));

        }
    }


    /** add supermarket to array **/
    private static void addSupermarket(Supermarket item) {
        SUPERMARKETS.add(item);
        SUPERMARKET_MAP.put(item.id, item);
    }

    public static Supermarket createSupermarket(int position, String title, String street, Double lat, Double longitude) {

        Supermarket newMarket = new Supermarket(position, String.valueOf(title), String.valueOf(street), lat, longitude);
        SUPERMARKETS.add(newMarket);
        SUPERMARKET_MAP.put(newMarket.id, newMarket);
        return newMarket;
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }

    public static class Supermarket {
        public final Integer id;
        public final String name;
        public final String street;
        public final Double latitude;
        public final Double longitude;

        public Supermarket(Integer id, String name, String street, Double latitude, Double longitude) {
            this.id = id;
            this.name = name;
            this.street = street;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public Integer getId() { return id; }
        public String getName() { return name; }
        public String getStreet() { return street; }
        public Double getLatitude() { return latitude; }
        public Double getlongitude() { return longitude; }

    }
}
