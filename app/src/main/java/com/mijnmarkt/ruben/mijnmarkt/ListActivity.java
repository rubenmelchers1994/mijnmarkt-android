package com.mijnmarkt.ruben.mijnmarkt;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.mijnmarkt.ruben.mijnmarkt.dummy.DummyContent;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private Supermarkets _supermarkets = new Supermarkets();
    private ArrayAdapter<Integer> _adapterID;
    private ArrayAdapter<String> _adapterTitle;
    private ArrayAdapter<String> _adapterStreet;
    private ArrayAdapter<Double> _adapterLat;
    private ArrayAdapter<Double> _adapterLong;
    public static final List<Supermarkets> SUPERMARKETS = new ArrayList<Supermarkets>();
    protected static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(count == 0) {
            new SupermarketDownloader(this).execute();
        }

        /** Initiate adapters **/
        _adapterID = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _supermarkets.getSupermarketID());
        _adapterTitle = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _supermarkets.getTitle());
        _adapterStreet = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _supermarkets.getStreet());
        _adapterLat = new ArrayAdapter<Double>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _supermarkets.getLatitude());
        _adapterLong = new ArrayAdapter<Double>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _supermarkets.getLongitude());
        count++;
    }

    /** Setup the recyclerview; fill supermarket listitems in the list activity **/
    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new MyItemRecyclerViewAdapter(DummyContent.SUPERMARKETS));
    }

    /** Fill the arrays with supermarket data **/
    public void setSupermarkets( Supermarkets supermarkets ) {
        _supermarkets = supermarkets;
        _adapterID.addAll(_supermarkets.getSupermarketID());
        _adapterTitle.addAll(_supermarkets.getTitle());
        _adapterStreet.addAll(_supermarkets.getStreet());
        _adapterLat.addAll(_supermarkets.getLatitude());
        _adapterLong.addAll(_supermarkets.getLongitude());

        _adapterID.notifyDataSetChanged();
        _adapterTitle.notifyDataSetChanged();
        _adapterStreet.notifyDataSetChanged();
        _adapterLat.notifyDataSetChanged();
        _adapterLong.notifyDataSetChanged();

        listFill();
    }

    /** Loop through arrays and call the createSupermarket function for each result **/
    public void listFill() {
        for(int i = 0; i < _adapterTitle.getCount(); i++) {
            DummyContent.createSupermarket(i, _adapterTitle.getItem(i), _adapterStreet.getItem(i), _adapterLat.getItem(i), _adapterLong.getItem(i));
        }

        /** Call the reyclerview setup, so the list is actually filled with the created supermarkets **/
        View recyclerview = findViewById(R.id.supermarket_list);
        assert recyclerview != null;
        setupRecyclerView((RecyclerView) recyclerview);
    }
}
