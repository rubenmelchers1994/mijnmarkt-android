package com.mijnmarkt.ruben.mijnmarkt;

import java.util.ArrayList;

public class Supermarkets {

    ArrayList<Integer> SupermarketID = new ArrayList<>();
    ArrayList<String> Title = new ArrayList<>();
    ArrayList<String> Street = new ArrayList<>();
    ArrayList<Double> Latitude = new ArrayList<>();
    ArrayList<Double> Longitude = new ArrayList<>();

    public ArrayList<Integer> getSupermarketID() {
        return SupermarketID;
    }

    public void setSupermarketID(ArrayList<Integer> supermarketID) {
        SupermarketID = supermarketID;
    }

    public ArrayList<String> getTitle() {
        return Title;
    }

    public void setTitle(ArrayList<String> title) {
        Title = title;
    }

    public ArrayList<String> getStreet() {
        return Street;
    }

    public void setStreet(ArrayList<String> street) {
        Street = street;
    }

    public ArrayList<Double> getLatitude(){
        return Latitude;
    }

    public void setLatitude(ArrayList<Double> latitude){
        Latitude = latitude;
    }

    public ArrayList<Double> getLongitude(){
        return Longitude;
    }

    public void setLongitude(ArrayList<Double> longitude){
        Longitude = longitude;
    }

}
