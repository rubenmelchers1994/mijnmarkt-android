package com.mijnmarkt.ruben.mijnmarkt;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.app.ActivityManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationServices;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleMap.OnMyLocationChangeListener {

    private GoogleMap mMap;

    Double MyLat;
    Double MyLong;
    Double _latitude;
    Double _longitude;
    String _title;
    private LocationManager locationManager;
    private GoogleApiClient mGoogleApiClient;
    private LocationCallback mLocationCallback;
    boolean mRequestingLocationUpdates;
    private LocationManager mFusedLocationClient;
    private long mLocationRequest;
    private int notificationId = 1;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /** get data from intent **/
        Intent intent = getIntent();
        _latitude = intent.getDoubleExtra("supermarket_latitude", 0);
        _longitude = intent.getDoubleExtra("supermarket_longitude", 0);
        _title = intent.getStringExtra("supermarket_title");

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {

                    Log.d("MAPS_DEBUG", "MOVING...");
                    // Update UI with location data
                    // ...
                }
            };
        };

      }


    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) this.getSystemService(this.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (Tracking.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /** Set coordinates for found supermarket **/
        LatLng supermarketPositition = new LatLng(_latitude, _longitude);
        mMap.addMarker(new MarkerOptions().position(supermarketPositition).title(_title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(supermarketPositition, 16.0f));

        /** start the tracking service for background location updates **/
        Intent Tracking = new Intent(this, Tracking.class);
        Tracking.putExtra("market_lat", _latitude);
        Tracking.putExtra("market_long", _longitude);
        Tracking.putExtra("market_title", _title);

        this.startService(Tracking);
        mMap.setOnMyLocationChangeListener(this);

    }

    /** Go back to detailview **/
    public void finish(View view) {
        finish();
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("MAPS_LOG", "ONCONNECTED");

        /** Check/ask for permission **/
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }

            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

                MyLat = mLastLocation.getLatitude();
                MyLong = mLastLocation.getLongitude();

                /** Set marker on own location **/
                LatLng myLoc = new LatLng(MyLat, MyLong);
                mMap.addMarker(new MarkerOptions().position(myLoc).title("MyLocation"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));

            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {

                MyLat = mLastLocation.getLatitude();
                MyLong = mLastLocation.getLongitude();

                /** Create new marker on own location when it has changed and move camera to it **/
                LatLng myLoc = new LatLng(MyLat, MyLong);
                Location myLocation = new Location("ownLocation");
                myLocation.setLatitude(MyLat);
                myLocation.setLongitude(MyLong);
                mMap.addMarker(new MarkerOptions().position(myLoc).title("MyLocation"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(myLoc));

                /** get supermarket location **/
                Location supermarketLocation = new Location("store");
                supermarketLocation.setLatitude(_latitude);
                supermarketLocation.setLongitude(_longitude);

                /** check if user is nearby. if so, send a toast message and a notification saying the user is almost there. if not, only send a toast message with the opposite information **/
                Boolean distanceMeter = checkDistance(myLocation, supermarketLocation);
                if(distanceMeter)
                {
                    Toast.makeText(this, "You are close by!", Toast.LENGTH_LONG).show();

                    Intent intent2 = new Intent(this, MapsActivity.class);
                    intent2.putExtra("supermarket_latitude", _latitude);
                    intent2.putExtra("supermarket_longitude", _longitude);
                    intent2.putExtra("supermarket_title", _title);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent2, 0);

                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1")
                            .setSmallIcon(R.drawable.img_walking)
                            .setContentTitle("Vlak bij " + _title)
                            .setContentText("Je bent er bijna!")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            // Set the intent that will fire when the user taps the notification
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

                    notificationManager.notify(notificationId, mBuilder.build());

                    notificationId++;
                    Log.d("DISTANCE", "within range!");
                }
                else{
                    Toast.makeText(this, "Too far away!", Toast.LENGTH_LONG).show();
                    Log.d("DISTANCE", "too far away");
                }
            }
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    /** Check distance method to check difference between two locations **/
    private Boolean checkDistance(Location startPoint, Location endpoint)
    {
        Boolean isClose = false;
        Location start=startPoint;
        Location end=endpoint;
        double distance=start.distanceTo(end);
        if(distance < 200f)
        {
            isClose = true;
        }
        return isClose;
    }

    /** When app is resumed, request new updates **/
    @Override
    protected void onResume() {
        super.onResume();

        if (mRequestingLocationUpdates) {

        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5, this);
        }
    }

    @Override
    protected void onPause() {
        locationManager.removeUpdates(this);
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("DEBUG", "permission granted");
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Log.d("DEBUG", "permission denied");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void onMyLocationChange(Location location) {
        Log.d("DEBUG_MAPS", "MOVING");
    }
}
