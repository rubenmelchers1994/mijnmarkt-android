package com.mijnmarkt.ruben.mijnmarkt;

import android.app.DownloadManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.api.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SupermarketDownloader extends AsyncTask<Void, Void, Supermarkets> {

    private HttpURLConnection mHttpUrl;

    private WeakReference<ListActivity> _listActivity;

    public SupermarketDownloader(ListActivity parent) {
        super();
        _listActivity = new WeakReference<ListActivity>(parent);
    }

    @Override
    protected Supermarkets doInBackground(Void... voids) {
        return getSupermarketData();
    }

    @Override
    protected void onPostExecute(Supermarkets result) {

        // AsyncTask finishes here

        // check if link with parent isn't lost
        // and if a location has been found before returning

        /** When the task is finished, send the results to the listactivity, which will handle the rest **/
        if (result != null && _listActivity.get() != null) {
            ListActivity listActivity = _listActivity.get();
            listActivity.setSupermarkets(result);

        } else {
            Log.d("API_LOG", "ON POST EXECUTE NO RESULTS");
        }
    }

    /** Connect to API, get data and then send it to the JSON method, which will handle the data and give it back **/
    private Supermarkets getSupermarketData() {
        String result = null;
        BufferedReader in = null;

        try {

            URL url;

            url = new URL("https://supermarket-api.rubenmelchers.nl/");
            mHttpUrl = (HttpURLConnection) url.openConnection();
            mHttpUrl.setRequestMethod("GET");
            mHttpUrl.setRequestProperty("Accept", "*/*");

            in = new BufferedReader(new InputStreamReader(
                    mHttpUrl.getInputStream()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            while ((line = in.readLine()) != null) {
                sb.append(line + "\n");
            }
            result = sb.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != in) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            mHttpUrl.disconnect();
        }
        return createJSONSupermarkets(result);

    }

    /** Create JSON from raw string received from API, loop through it and create a supermarket object **/
    private Supermarkets createJSONSupermarkets(String result) {

        Supermarkets _supermarket = new Supermarkets();

        try {
            JSONArray supermarkets = new JSONArray(result);

            for (int i = 0; i < supermarkets.length(); i++) {
                JSONObject row = supermarkets.getJSONObject(i);

                /** map each found result to the corresponding supermarket data **/
                _supermarket.getSupermarketID().add(row.getInt("id"));
                _supermarket.getTitle().add(row.getString("title"));
                _supermarket.getStreet().add(row.getString("street"));
                _supermarket.getLatitude().add(row.getDouble("latitude"));
                _supermarket.getLongitude().add(row.getDouble("longitude"));


            }
        } catch (JSONException e) {
            /** Wrong JSON format probably **/

            e.printStackTrace();
            _supermarket = new Supermarkets();
        }

        /** return the created supermarket object **/
        return _supermarket;
    }


}
