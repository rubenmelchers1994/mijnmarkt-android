package com.mijnmarkt.ruben.mijnmarkt;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toolbar;

import com.google.android.gms.maps.SupportMapFragment;
import com.mijnmarkt.ruben.mijnmarkt.dummy.DummyContent;;

public class MainActivity extends AppCompatActivity implements ItemFragment.OnListFragmentInteractionListener {

    private Supermarkets _Supermarkets = new Supermarkets();

    private ArrayAdapter<String> _adapterID;
    private ArrayAdapter<String> _adapterTitle;
    private ArrayAdapter<String> _adapterStreet;
    private ArrayAdapter<Double> _adapterLat;
    private ArrayAdapter<Double> _adapterLong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        new SupermarketDownloader(this).execute();

//        _adapterID = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, android.R.id.Integ, _Supermarkets.getSupermarketID());
        _adapterTitle = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _Supermarkets.getTitle());
        _adapterStreet = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _Supermarkets.getStreet());
        _adapterLat = new ArrayAdapter<Double>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _Supermarkets.getLatitude());
        _adapterLong = new ArrayAdapter<Double>(this, android.R.layout.simple_list_item_1, android.R.id.text1, _Supermarkets.getLongitude());


//        View recyvlerView = findViewById(R.id.supermarket_list);
//        Toolbar toolbar = (Toolbar) findViewById();
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);


    }

    @Override
    public void onListFragmentInteraction(DummyContent.Supermarket item) {

        Log.d("DEBUG2", "HOI");
        Log.d("item", "" + item);

        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("item_id", item.id);
        startActivity(intent);

    }

//    public void setSupermarkets( Supermarkets supermarkets ) {
//
//    }
}
